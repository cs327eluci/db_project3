-- Generated by Oracle SQL Developer Data Modeler 4.1.3.901
--   at:        2016-10-22 18:37:29 CDT
--   site:      Oracle Database 11g
--   type:      Oracle Database 11g




DROP TABLE employment CASCADE CONSTRAINTS ;

DROP TABLE hos_name CASCADE CONSTRAINTS ;

DROP TABLE hospital CASCADE CONSTRAINTS ;

DROP TABLE patient CASCADE CONSTRAINTS ;

DROP TABLE patient_history CASCADE CONSTRAINTS ;

DROP TABLE provider CASCADE CONSTRAINTS ;

DROP TABLE visit CASCADE CONSTRAINTS ;

CREATE TABLE employment
  (
    emp_id      INTEGER NOT NULL ,
    provider_id INTEGER NOT NULL ,
    hospital_id INTEGER NOT NULL
  ) ;
ALTER TABLE employment ADD CONSTRAINT prov_hosp_PK PRIMARY KEY ( emp_id ) ;


CREATE TABLE hos_name
  ( hos_name_id INTEGER NOT NULL , name VARCHAR2 (256)
  ) ;
ALTER TABLE hos_name ADD CONSTRAINT hos_name_PK PRIMARY KEY ( hos_name_id ) ;


CREATE TABLE hospital
  (
    hospital_id INTEGER NOT NULL ,
    location    VARCHAR2 (256) ,
    hos_name_id INTEGER NOT NULL
  ) ;
CREATE UNIQUE INDEX hospital__IDX ON hospital
  (
    hos_name_id ASC
  )
  ;
ALTER TABLE hospital ADD CONSTRAINT hospital_PK PRIMARY KEY ( hospital_id ) ;


CREATE TABLE patient
  (
    patient_id   INTEGER NOT NULL ,
    name         VARCHAR2 (256) ,
    organ_donor  CHAR (1) ,
    release_date VARCHAR2 (256) ,
    type         VARCHAR2 (9) NOT NULL
  ) ;
ALTER TABLE patient ADD CONSTRAINT CH_INH_patient CHECK ( type IN ('fatal', 'non_fatal', 'patient')) ;
ALTER TABLE patient ADD CONSTRAINT patient_ExDep CHECK ( (type = 'fatal' AND release_date IS NULL) OR (type = 'non_fatal' AND organ_donor IS NULL) OR (type = 'patient' AND release_date IS NULL AND organ_donor IS NULL)) ;
ALTER TABLE patient ADD CONSTRAINT patient_PK PRIMARY KEY ( patient_id ) ;


CREATE TABLE patient_history
  (
    patient_history_id  INTEGER NOT NULL ,
    high_blood_pressure CHAR (1) ,
    smoker              CHAR (1) ,
    high_cholesterol    CHAR (1) ,
    patient_id          INTEGER NOT NULL
  ) ;
CREATE UNIQUE INDEX patient_history__IDX ON patient_history
  (
    patient_id ASC
  )
  ;
ALTER TABLE patient_history ADD CONSTRAINT patient_history_PK PRIMARY KEY ( patient_history_id ) ;


CREATE TABLE provider
  (
    provider_id INTEGER NOT NULL ,
    name        VARCHAR2 (256) ,
    position    VARCHAR2 (256)
  ) ;
ALTER TABLE provider ADD CONSTRAINT provider_PK PRIMARY KEY ( provider_id ) ;


CREATE TABLE visit
  (
    visit_id    INTEGER NOT NULL ,
    hospital_id INTEGER NOT NULL ,
    provider_id INTEGER NOT NULL ,
    patient_id  INTEGER NOT NULL
  ) ;
ALTER TABLE visit ADD CONSTRAINT visit_PK PRIMARY KEY ( visit_id ) ;


ALTER TABLE visit ADD CONSTRAINT Relation_10 FOREIGN KEY ( patient_id ) REFERENCES patient ( patient_id ) ;

ALTER TABLE patient_history ADD CONSTRAINT Relation_11 FOREIGN KEY ( patient_id ) REFERENCES patient ( patient_id ) ;

ALTER TABLE employment ADD CONSTRAINT r10 FOREIGN KEY ( hospital_id ) REFERENCES hospital ( hospital_id ) ;

ALTER TABLE visit ADD CONSTRAINT r2 FOREIGN KEY ( hospital_id ) REFERENCES hospital ( hospital_id ) ;

ALTER TABLE hospital ADD CONSTRAINT r7 FOREIGN KEY ( hos_name_id ) REFERENCES hos_name ( hos_name_id ) ;

ALTER TABLE visit ADD CONSTRAINT r8 FOREIGN KEY ( provider_id ) REFERENCES provider ( provider_id ) ;

ALTER TABLE employment ADD CONSTRAINT r9 FOREIGN KEY ( provider_id ) REFERENCES provider ( provider_id ) ;


-- Oracle SQL Developer Data Modeler Summary Report: 
-- 
-- CREATE TABLE                             7
-- CREATE INDEX                             2
-- ALTER TABLE                             16
-- CREATE VIEW                              0
-- ALTER VIEW                               0
-- CREATE PACKAGE                           0
-- CREATE PACKAGE BODY                      0
-- CREATE PROCEDURE                         0
-- CREATE FUNCTION                          0
-- CREATE TRIGGER                           0
-- ALTER TRIGGER                            0
-- CREATE COLLECTION TYPE                   0
-- CREATE STRUCTURED TYPE                   0
-- CREATE STRUCTURED TYPE BODY              0
-- CREATE CLUSTER                           0
-- CREATE CONTEXT                           0
-- CREATE DATABASE                          0
-- CREATE DIMENSION                         0
-- CREATE DIRECTORY                         0
-- CREATE DISK GROUP                        0
-- CREATE ROLE                              0
-- CREATE ROLLBACK SEGMENT                  0
-- CREATE SEQUENCE                          0
-- CREATE MATERIALIZED VIEW                 0
-- CREATE SYNONYM                           0
-- CREATE TABLESPACE                        0
-- CREATE USER                              0
-- 
-- DROP TABLESPACE                          0
-- DROP DATABASE                            0
-- 
-- REDACTION POLICY                         0
-- 
-- ORDS DROP SCHEMA                         0
-- ORDS ENABLE SCHEMA                       0
-- ORDS ENABLE OBJECT                       0
-- 
-- ERRORS                                   0
-- WARNINGS                                 0
